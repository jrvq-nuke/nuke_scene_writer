# Nuke scene writer

A widget (Qtwidget) that looks through the whole Nuke scene and retrieves all ScanlineRender, RayRender and Write nodes. It then lets the user customize them before writing, without changing any of their original settings.

### Recommended way to launch
Install the module to your desired location and run:
```
from nuke_scene_writer import scene_writerUI
sr = scene_writerUI.SceneWriter()
```

### Look of the widget
![widget](http://www.jaimervq.com/wp-content/uploads/2020/05/nuke_scene_writer.jpg)

This module has been tested successfully in **Nuke 11.1v1**
***

For more info: www.jaimervq.com